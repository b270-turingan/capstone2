const mongoose = require("mongoose");

const ordersSchema = new mongoose.Schema({

	email: {
		type: String,
		required: [true, "Email is required."]
	},

	product: [{

		name: {
			type: String,
			required: [true, "Product name is required"]
		},

		price: {
			type: Number,
			default: 0
		},

		quantity: {
			type: Number,
			default: 0
		}	
	}],

	totalAmount: {
		type: Number,
		default: 0
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},

	isPaid: {
		type: Boolean,
		default: false
	}
});

module.exports = mongoose.model("Orders", ordersSchema);