const mongoose = require("mongoose");


const usersSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},

	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	contactNumber: {
		type: String,
		required: [true, "Contact Number is required."]
	},

	password: {
		type: String,
		required: [true, "Password in required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	orders: [{
			
			product: [{

				name: {
					type: String,
					required: [true, "Product name is required."]
				},

				price: {
					type: Number,
					default: 0
				},

				quantity: {
					type: Number,
					default: 0
				}
			}],

			totalAmount: {
				type: Number,
				default: 0
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			},

			isPaid: {
				type: Boolean,
				default: false
			}
		}],

	addToCart: [{

	    product: [{

	      name: {
	        type: String,
	        required: [true, "Product name is required."]
	      },

	      price: {
	        type: Number,
	        default: 0
	      },

	      quantity: {
	        type: Number,
	        default: 0
	      }
	    }],
	    
	    totalAmount: {
	      type: Number,
	      default: 0
	    }
	  }]
	});

module.exports = mongoose.model("Users", usersSchema);