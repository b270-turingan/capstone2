const mongoose = require("mongoose");

const productsSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ["Coffee", "Fruit Tea", "Snacks"],
    required: [true, "Type is required"],
  },
  name: {
    type: String,
    required: [true, "Name is required"],
  },
  description: {
    type: String,
    required: [true, "Description is required"],
  },
  sold: {
    type: Number,
    default: 0,
  },
  price: {
    type: Number,
    required: [true, "Price is required"],
  },
  isActive: {
    type: Boolean,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  profileImage: {
    type: String,
  },
});

module.exports = mongoose.model("Products", productsSchema);
