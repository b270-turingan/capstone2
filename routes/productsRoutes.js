const express = require("express");
const router = express.Router();
const productsController = require("../controllers/productsController");
const auth = require("../auth");


// route for creating or adding a product
router.post("/newProduct", auth.verify, productsController.newProduct);

// route for retrieving all active products
router.get("/activeProducts", productsController.activeProducts);

router.get("/all", productsController.getAllProducts);

// route for retrieving a single product
router.get("/:productsId", productsController.getProduct);

// route for updating a product info
router.put("/edit/:productsId", auth.verify, productsController.updateProduct);

// route for archiving a single product
router.patch("/archive/:productsId", auth.verify, productsController.archiveProduct);


module.exports = router;

