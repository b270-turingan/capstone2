const express = require("express");
const router = express.Router();
const auth = require("../auth");
const usersController = require("../controllers/userController");

// Route for user registration
router.post("/register", usersController.registerUser);

// Route for logging in
router.post("/login", usersController.userLogin);

// Route for retrieving ALL Users - Details
router.get("/all", usersController.allUsers);

// Route for create order
router.post("/checkOut", auth.verify, usersController.createOrder);

// Route for retrieving User Details
router.get("/:userId", usersController.userDetails);

// Route for retrieving ALL Users - Details
router.get("/", usersController.getAllUsers);

// Route for setting the user as admin
router.patch("/information/:userId", auth.verify, usersController.setAdmin);

// Route for Add to Cart
router.post("/addToCart", auth.verify, usersController.addToCart);

// Route for deleting a user.
router.delete("/delete/:userId", auth.verify, usersController.deleteUser);



module.exports = router;