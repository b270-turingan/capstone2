const express = require("express");
const router = express.Router();
const ordersController = require("../controllers/ordersController");
const auth = require("../auth");

// Route for retrieving all orders.
router.get("/", auth.verify, ordersController.getAllOrders);

// Route for updating Paid orders.
router.patch("/pendingOrders/:ordersId", auth.verify, ordersController.updateOrders);

// Route for retrieving pending orders
router.get("/pendingOrders", auth.verify, ordersController.pendingPayment);

// Route for retrieving authenticaed user's order
router.get("/list", auth.verify, ordersController.retrieveOrder);

// Route for deleting orders
router.delete("/list/deleteOrder/:ordersId", ordersController.deleteOrder)


module.exports = router;