const User = require("../models/Users");
const Products = require("../models/Products");
const Orders = require("../models/Orders");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// User Registration
/*
  - The system will check if the email is already exists
  - The system will register the user if the email provided is not existing.
*/
module.exports.registerUser = (req, res) => {

  let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    contactNumber: req.body.contactNumber,
    password: bcrypt.hashSync(req.body.password, 10)
  })

    User.findOne({email: req.body.email}).then(existingUser => {
      if (existingUser){
      res.send({message: "Email already exists"})
      } else{
        return newUser.save().then(user => {
          console.log(user);
          res.send({message: `Hello ${newUser.firstName}! Welcome to Brew Voyage Cafe.`})
        })
        .catch(error => {
          console.log(error);
          res.send(false);
        })
      }
    })
}

// User Log in
/*
  - If the user is null or not registered, a message will appear to update that there is no user existing.
  - If the password is incorrect, a message will appear to update that the password is incorrect. 
*/

module.exports.userLogin = (req, res) => {

  return User.findOne({email: req.body.email}).then(result => {

    // User does not exist
    if(result == null) {

      return res.send({message: "No user found"});
    // User exists
    } else {

      const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

      if(isPasswordCorrect) {
        // Generate an access token by invoking the "createAccessToken" in the auth.js file
        return res.send({accessToken: auth.createAccessToken(result)});

      // Password do not match
      } else {
        return res.send({message: "Incorrect Password"})
      }
    }
  })
}

module.exports.allUsers = (req, res) => {

  return User.find()
  .sort({firstName: 1})
  .select("-orders")
  .select("-password")
  .select("-addToCart")
  .then(result => {

    console.log(result);
    res.send(result);
  })
  .catch(error => {
    console.log(error);
    res.send(error);
  })
}



// Create Order
/*
  - Products Added by Non-admin users.
  - Insert Product quantities
  - Total Amount of orders.
*/

module.exports.createOrder = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  let orderList = [];
  let totalAmount = 0;

  if (!userData.isAdmin) {
    try {
      for (let orderProduct of req.body.products) {
        let productData = await Products.findById(orderProduct.productId);

        if (!productData) {
          return res.status(404).json({ message: 'Product not found' });
        }

        productData.sold += orderProduct.quantity;
        orderList.push({
          name: productData.name,
          price: productData.price,
          quantity: orderProduct.quantity,
          sold: productData.sold,
        });

        totalAmount += orderProduct.quantity * productData.price;

        await productData.save();
      }

      let newOrder = new Orders({
        userId: userData.id,
        email: userData.email,
        product: orderList,
        totalAmount: totalAmount,
        isPaid: userData.isPaid,
        purchasedOn: userData.purchasedOn,
      });

      await newOrder.save();

      const updatedUser = await User.findByIdAndUpdate(
        userData.id,
        { $push: { orders: newOrder } },
        { new: true }
      );

      return res.json({
        message: `Hello ${updatedUser.firstName}, your total payment is: \u20B1${totalAmount}.`,
      });
    } catch (error) {
      console.log(`Order creation failed: ${error}`);
      return res.status(500).json({ message: 'Order creation failed' });
    }
  } else {
    return res.json(false);
  }
};


// Retrieving User Details
/*
  - Getting the information of the specific user
  - For Data Privacy, The system exclude the "email, password, contact Number and orders" to protect the user's personal information.
*/

module.exports.userDetails = (req, res) => {

  return User.findById(req.params.userId)

  .select("-orders")
  .select("-email")
  .select("-password")
  .select("-contactNumber")
  .select("-addToCart")
  .then(result => {


    console.log(result);
    res.send(result);
  })
  .catch(error => {
    console.log(error);
    res.send(error);
  })
}

// Retrieving All Users
/*
  - Retrieve all Users
  - For the users' data privacy, the system exclude the email, password, contact number and their orders.
  - It is in alphabetical order sort by their first name.
*/

module.exports.getAllUsers = async (req,res) => { 
      const userData = auth.decode (req.headers.authorization);///    
      //return User.findById(req.params.userId)
      return User.findById(userData.id)
        .then(result => res.send (console.log (result) || result))  
        .catch (error => res.send(console.log(error) || error));
    }


// Set user as admin
/*
  - Only admin can set the user as admin.
*/

module.exports.setAdmin = (req, res) => {

  const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin) {
      let setAdmin = {
        isAdmin: req.body.isAdmin
      }

      return User.findByIdAndUpdate(req.params.userId, setAdmin, {new:true}).then(result => {

        if(setAdmin.isAdmin){
          console.log(result);
          return User.findById(req.params.userId).then(user => {
                      res.send({ message: `You set ${user.firstName} as admin.` });
                    });
        } else {
          console.log(result);
          return User.findById(req.params.userId).then(user => {
                      res.send({ message: `You removed ${user.firstName} as admin.` });
                    });
        }
      })
      .catch(error => {
        console.log(error);
        res.send(false);
      })
    } else {
      return res.send(false)
    }

}

// Add to Cart

module.exports.addToCart = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (!userData.isAdmin) {
    let productList = [];
    let totalAmount = 0;

    try {
      for (let orderProduct of req.body.products) {
        let productData = await Products.findById(orderProduct.productId);

        if (!productData) {
          return res.status(404).json({ message: 'Product not found' });
        }

        productData.quantity -= orderProduct.quantity;
        await productData.save();
        productList.push({
          name: productData.name,
          price: productData.price,
          quantity: orderProduct.quantity,
        });
        totalAmount += productData.price * orderProduct.quantity;
      }

      userData.addToCart = [
        {
          product: productList,
          totalAmount: totalAmount,
        },
      ];

      const user = await User.findOneAndUpdate(
        { _id: userData.id },
        { addToCart: userData.addToCart },
        { new: true }
      );

      const firstName = user.firstName; // Extracting the first name from the user object

      console.log(`User Data Saving SUCCESSFUL ${user}`);
      return res.send(
        `Hello ${firstName}, your selected product has been saved to the cart. \nTotal Payment: \u20B1${totalAmount}`
      );
    } catch (error) {
      console.log(`Add to cart failed: ${error}`);
      return res.status(500).json({ message: 'Add to cart failed' });
    }
  }

  return res.send(false);
};


// Deleting a user:

module.exports.deleteUser = async (req, res) => {
  const token = req.headers.authorization;

  if (!token) {
    return res.status(401).json({ message: 'Authorization token not provided.' });
  }

  try {
    const userData = auth.decode(token);
    
    if (!userData.isAdmin) {
      return res.status(403).json({ message: 'Only admin users can delete users.' });
    }
    
    const user = await User.findByIdAndDelete(req.params.userId);
    
    if (!user) {
      return res.status(404).json({ message: 'User not found.' });
    }
    
    return res.status(200).json({ message: 'User deleted successfully.' });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: 'An error occurred while deleting the user.' });
  }
};