const Products = require("../models/Products");
const auth = require("../auth");

// For Adding a new product
/*	
	- Only admin can add a product
	- The Product type is strict to 3 types: Coffee, Fruit Tea and Snacks, If the product is not the same with the strict 3 types, it will not add the product.
	- If it is existing product, it will not add and there will a error message.
*/

module.exports.newProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	let addProduct = new Products({

		type: req.body.type,
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive: req.body.isActive
	})

	if(addProduct.type !== "Coffee" && addProduct.type !== "Fruit Tea" && addProduct.type !== "Snacks"){

		res.send("Please enter the correct PRODUCT TYPE.")
	} else {
		Products.findOne({type: req.body.type} && {name: req.body.name})
		.then(existingProduct => {
			if(existingProduct){
				res.send({message: `${addProduct.type}: ${addProduct.name} is already exists. Add a different product`})
			} else {
				// Only admin have the authority to add a new product. 
					if(true !== userData.isAdmin){
						return res.send(false);
					} else {
						return addProduct.save().then(products => {
							console.log(products);
							res.send({message: `${products.type}: ${products.name} is added.`})
						})
						.catch(error => {
							console.log(error);
							res.send(false);
						})
				}
			}
		})
	}
}

// Retrieving all Active Products
/*
	- Retrieve all ACTIVE Products
	- Sorted by what product has the highest sales.
*/

module.exports.activeProducts = (req, res) => {

	return Products.find({isActive: true}).sort({sold: -1}).then(result => res.send(result));
}

// Retrieving single product
/*
	- It will retrieve the specific product that is added in url
*/
module.exports.getProduct = (req, res) => {
  Products.findById(req.params.productsId)
    .then((result) => {
      console.log(result);
      return res.send(result);
    })
    .catch((error) => {
      console.log(error);
      res.send(error);
    });
};


// Updating the information of the Product
/*
	- Only admin can update the information of a specific Product, that is inserted in url
*/

module.exports.updateProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){

		let updateProduct = {

			type: req.body.type,
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
		}

		return Products.findByIdAndUpdate(req.params.productsId, updateProduct, {new: true}).then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(error);
		})
	} else {
		return res.send(false);
		console.log(false);
	}
}

module.exports.getAllProducts = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		return Products.find({}).then(result => res.send(result));
	} else {
		return res.send(false);
	}
	
}


// Archiving Product
/*
	- Only Admin can Archive a product that is inserted in url
*/

module.exports.archiveProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin){

		let archiveProduct = {
			isActive: req.body.isActive
		}

		return Products.findByIdAndUpdate(req.params.productsId, archiveProduct, {new:true}).then(result => {
			if(archiveProduct.isActive === false){
				res.send({message: `Product is updated to Not Available`, result})
			} else {
				res.send({message: `Product is now Available`, result})
			}
		})
		.catch(error => {
			console.log(error);
			res.send(error)
		})
	} else {
		return res.send(false);
		console.log(false);
	}
}
