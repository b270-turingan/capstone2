const Orders = require("../models/Orders");
const Products = require("../models/Products")
const User = require("../models/Users");
const auth = require("../auth.js");


// Retrieving all orders.
/*
	- Only admin can retrieve all orders of users
*/

module.exports.getAllOrders = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		return Orders.find().then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(error);
		})
	} else {
		console.log(false);
		res.send(false);
	}
}

// Updating paid orders.
/*
	- The admin can change if the user is already paid on their orders.
	- It will also affect and change in the order's model as well as in user's model
*/

module.exports.updateOrders = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    try {
      const updateOrders = {
        isPaid: req.body.isPaid
      };

      const updatedOrder = await Orders.findByIdAndUpdate(
        req.params.ordersId,
        updateOrders,
        { new: true }
      );

      if (updatedOrder.isPaid) {
        const user = await User.findOne({ email: updatedOrder.email });
        if (user) {
          const order = user.orders.find(
            (order) => order._id.toString() === req.params.ordersId
          );

          if (order) {
            order.isPaid = true;
            await user.save();
          }
        }
      } else {
        const user = await User.findOne({ email: updatedOrder.email });
        if (user) {
          const order = user.orders.find(
            (order) => order._id.toString() === req.params.ordersId
          );

          if (order) {
            order.isPaid = false;
            await user.save();
          }
        }
      }

      console.log(updatedOrder);
      res.send(updatedOrder);
    } catch (error) {
      console.log(error);
      res.send(error);
    }
  } else {
    console.log(false);
    res.send(false);
  }
};


// Retrieving all orders who are not yet paid.

module.exports.pendingPayment = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		return Orders.find({isPaid: false}).then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			console.log(error);
		})
	} else {
		console.log(false);
		res.send(false);
	}
}

// Retrieve authenticated user's orders.
/*
	- It will retrieve the user's order. (JUST ORDERS)
*/


module.exports.retrieveOrder = (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (!userData.isAdmin) {
    User.findById(userData.id)
      .populate("orders.product", "name price quantity") // Populate the product details in the orders array
      .then((user) => {
        console.log(user.orders);
        res.send({ message: `${user.firstName} ${user.lastName}'s Order List:`, orders: user.orders });
      })
      .catch((error) => {
        console.log(error);
        res.send(error);
      });
  } else {
    console.log(false);
    res.send(false);
  }
};


// Delete an order.
// This will delete the selected order and will take effect on order's model and user's orders mddel.

module.exports.deleteOrder = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (!userData.isAdmin) {
    const ordersId = req.params.ordersId;
    const userId = userData.id;

    try {
      const deletedOrder = await Orders.findByIdAndDelete(ordersId);

      if (!deletedOrder) {
        return res.status(404).send({ message: 'Order not found.' });
      }

      await User.findByIdAndUpdate(userId, { $pull: { orders: { _id: ordersId } } });

      res.send({ message: 'Order deleted successfully.' });
    } catch (error) {
      console.log(error);
      res.status(500).send({ message: 'Error deleting the order.' });
    }
  } else {
    console.log(false);
    res.send(false);
  }
};